package com.gitlab.krystiankowalik.sample.cloud.config.client.starter;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:jasypt-default.properties")
public class JasyptPropertiesLoader {
}
